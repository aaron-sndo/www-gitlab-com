---
layout: handbook-page-toc
title: "Accounts Payable"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## <i class="far fa-paper-plane" id="biz-tech-icons"></i> Introduction

Welcome to the Accounts Payable page! You should be able to find answers to most of your questions here. IF you can't find what you are looking for, then:
- **Chat Channel**: `#accountspayable`
- **Email**: `ap@gitlab.com`


## <i class="fas fa-stream" id="biz-tech-icons"></i> QuickLinks

<div class="flex-row" markdown="0" style="height:110px;">
  <a href="/handbook/finance/accounting/#procure-to-pay/" class="btn cta-btn ghost-purple" style="width:250px;margin:5px;display:flex;align-items:center;height:100%;"><span>Invoicing and Payment</span></a> 
[Corporate Credit Card](https://about.gitlab.com/handbook/finance/accounting/#credit-card-use-policy) 
[Procurment]()







## Procure to Pay Process
GitLab currently uses procurement issues as the first step in the process. 
Next a vendor will be set up in our invoice processing and payment system - Tipalti.

Further details on this process can be found at [the Procure to Pay page](handbook/finance/accounting/#procure-to-pay)


#### Time to Process Invoices in Tipalti <= 2 business days
The time to submit invoices to the business for approval in Tipalti. The target is <= 2 business days.


## <i class="far fa-flag" id="biz-tech-icons"></i> Coupa is coming!
We’re excited to announce that GitLab will be launching Coupa in June!

Coupa is a procure-to-pay system that will help us streamline the purchase request process, initiate workflow with approvals, and enable Purchase Orders. We will be rolling out in a phased approach, starting with the US and Netherlands entities - GitLab Inc, Federal, IT BV and BV.

You can learn more about Coupa in our [FAQ Page](/handbook/finance/procurement/coupa-faq/)


#### Vendor Code of Ethics
All vendors that GitLab does business with, must legally comply with the [Supplier Code of Ethics](/handbook/people-group/people-policy-directory/#partner-code-of-ethics). When having discussions with your vendor regarding the contract, make them aware of this requirement.


### Accounts Payable Performance Indicators


