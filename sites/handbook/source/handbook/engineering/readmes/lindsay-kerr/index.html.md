---
layout: markdown_page
title: "Lindsay Kerr's README"
job: "Frontend Engineering Manager, Threat Management"
---

## Lindsay Kerr's README

Hello, I'm Lindsay. I am the Frontend Engineering Manager for the Threat Insights (Secure stage) and Container Security (Protect stage) groups here at GitLab. I made this README as a way for others to get to know me better. I have not worked with any of my colleagues at GitLab before, or at this point met them in person, so this README is one way to better introduce myself. This is the first README I've ever written, it admittedly feels a little awkward. In an effort to start somewhere, I'm practicing the value of 👣 [iteration](/handbook/values/#iteration) & plan to update this README frequently.

## About me

I live in Eugene, Oregon with my husband, our six year old daughter Audrey, and two cats (Takahata & Miyazaki). If you couldn't guess based on our cat's names, we love watching Studio Ghibli movies. I also enjoy gardening, hiking, kayaking, road-trips, and listening to music. I was born and raised in Portland, Oregon. After moving around to a few places (including Chicago & San Fransisco) we settled down & bought our "purple palace" in Eugene. It is actually purple, but it's only a palace to us.

## My working style

* I subscribe to the [servant-leadership](https://en.wikipedia.org/wiki/Servant_leadership) style. 
* I try to present solutions or suggestions whenever bringing a complaint or concern to a discussion.
* I embrace transparency & asynchronous communication whenever possible.
* I work hard to remove distractions for engineers on my team. My goal is to allow them to focus on solving technical  problems as much as possible. As a manager, I'm here to clarify, unblock, cheerlead, promote, and encourage improvements.
* Scrum and Agile development practices are what brought me into management. I am passionate about building strong team processes.

## What I assume about others

* I put confidence in other people's positive intentions, people mean well and strive to do their best. 
* We are all on one team working together to achieve common goals.
* Everyone wants to have a little bit of fun at work. 
* Honesty is the foundation of all conversations. 

## Communicating with me

* My working hours are typically 9-5pm PST Monday-Friday.
* I avoid checking Slack & email over the weekend or in the evening. 
* If your topic to discuss relates to something being tracked in an issue or MR, whenever possible start the conversation there.
* Don't hesitate to reach out to me directly if you want to talk about anything personal, sensitive, or confidential.
* I appreciate honesty even when it involves criticism.

### 1:1s

* I may reschedule, you should feel free to too, but I avoid cancelling if at all possible.
* We can talk about most work related topics in the MR, issue, or Slack. 1:1s are best spent getting to know each other better.
* I tend over share about my life. 
* Agendas should be driven by my direct reports. But if you don't add anything, we're still talking.
* I have a tendency of cursing & telling inappropriate stories/jokes about myself.

### Personality quirks

* My face turns red when I'm embarrassed, excited, nervous ... and at other unexpected times. 
* I have a standing desk but can't stay standing (or sitting) for more than 10 minutes at a time, if you're on a long call with me expect to see my desk adjusted multiple times.
* The sound of me blowing my nose has been compared to that of a baby elephant. 

## Let's connect on Social Media

* [LinkedIn](https://www.linkedin.com/in/lindsay-a-kerr/)
* [Twittter](https://twitter.com/lkerr78)
* [Instagram](https://www.instagram.com/lkerr_uo/)
