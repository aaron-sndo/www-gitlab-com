---
layout: handbook-page-toc
title: "Professional Services Engagement Management"
description: "Describes the workflow and responsibilities of the GitLab Professional Services Engagement Manager."

---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Professional Services Engagement Management

## Role of the Engagement Manager

The [Professional Services (PS) Engagement Manager](https://about.gitlab.com/job-families/sales/job-professional-services-engagement-manager/) is responsible for helping Solution Architects (SAs) and the sales team develop and gain approval for custom Statement of Works (SoWs). The process typically includes the following activities.

* An initial meeting with the SA or Customer to understand the objective and/or specific requirements
* Develop an initial Estimate based upon the Services Calculator
* Update the Estimate based upon initial feedback from the Customer and/or Sales team
* Once agreed, generate a supporting Statement of Work (SoW)
* Create a cost estimate (COGS)
* Providing clarification to the PS Director in order to obtain approval for the SoW
* Present the SoW to the account team to gain signature
* Track progress of the SoW through to signature
* Provide soft hand-off of the SOW to the Delivery team upon signature to ensure a smooth transition. 


## Meet the Team

- **Russell Knight**, Senior Professional Services Engagement Manager, EMEA
- **Julie Byrne**, Senior Professional Services Engagement Manager, US

## How to contact or collaborate with us

- Create a scoping issue using the [Services Calculator](https://services-calculator.gitlab.io)
- Slack: `#professional-services`
- Salesforce Chatter

## Useful Resources

- [Services to Accelerate Customer Adoption](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/sales-enablement/)
- [Selling Professional Services](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/selling/)
- [Professional Services Offering Framework](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/framework/)
- [Services Calculator](https://services-calculator.gitlab.io)

## Engagement Manager - Processes

### Salesforce - Tracking Professional Services Opportunities

Tracking the overall pipeline for Professional Services and individual opportunities is important to the Professional Services team to retain visbility and ensure capacity for future projects.
This importance increases as we approach the end of the financial quarter (FQ), as we typically see a spike in **Closed Won** deals at this time.

To help this visibilty, the Engagement Managers maintain a subset of fields against an opportunity under the `Professional Services` section. These fields are reviewed and updated on a regular basis to support reporting to Professional Services leadership on the current forecast:

- `Engagement Manager` - Denotes the Lead Engagement Manager supporting the opportunity
- `EM Confidence`, this can be set as:
    - **Grey** (Default), meaning the EM does not have visibility or awareness of the opportunity
    - **Red**, meaning in the opinion of the EM, this will not close won within the timeframe stated
    - **Amber**, meaning the EM is confident the oportunity will close won, but not necessarily within the timeframe stated
    - **Green**, meaning the EM is condient that the opportunity will close won within the time started
- `EM Confidence Rationale` - Context and rationale to support the `EM Confidence` field
- `Scoping Issue Link` - A direct link to the supporting scoping issue

