---
layout: handbook-page-toc
title: "Sales Assistance from Investors"
description: "This page overviews how GitLab's investors and board members can assist in deals"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

The below page overviews how GitLab's investors and board members can assist in deals.

# Requesting Assistance/Introductions Into Prospects from our Investors

We have great investors who can and want to help us penetrate strategic accounts.
Please discuss with your Manager details of your request and fill out [this form](https://forms.gle/vbwjWr5sUN6Z35Rh7). Your request will be reviewed for completeness and facilitated directly to the Investor or included in the monthly investor update section called "Asks".
All requests should be made before the 1st of the month so they are included in the upcoming investor update.

If an investor can help, they will contact the CRO, and the CRO will introduce the salesperson and the investor.
The salesperson shall ask how the investor wants to be updated on progress and follow up accordingly.

## Email Intro

_This introduction email is progressive. You can choose to use just the first paragraph or the first two, three, or four depending on the detail you feel is appropriate for your audience._

GitLab is a complete DevOps platform, delivered as a single application, fundamentally changing the way Development, Security, and Ops teams collaborate.
GitLab helps teams accelerate software delivery from weeks to minutes, reduce development costs, and reduce the risk of application vulnerabilities while increasing developer productivity.
GitLab provides unmatched visibility, radical new levels of efficiency and comprehensive governance to significantly compress the time between planning a change and monitoring its effect.

GitLab collapses cycle times by driving higher efficiency across all stages of the software development lifecycle.
For the first time, Product, Development, QA, Security, and Operations teams can collaborate in a single application. There’s no need to integrate and synchronize tools, or waste time waiting for handoffs.
Everyone contributes to a single conversation, instead of managing multiple threads across disparate tools. Development teams have complete visibility across the lifecycle with a single, trusted source of data to simplify troubleshooting and drive accountability.
All activity is governed by consistent controls, making security and compliance first-class citizens instead of an afterthought.

Built on Open Source, GitLab leverages the community contributions of thousands of developers and millions of users to continuously deliver new DevOps innovations.
More than 100,000 organizations from startups to global enterprise organizations, including Ticketmaster, Jaguar Land Rover, NASDAQ, Dish Network and Comcast trust GitLab to deliver great software at new speeds.

Some additional detail is linked below:

* [GitLab ranked number 4 software company (44th overall) on Inc. 5000 list of 2018's Fastest Growing Companies](/is-it-any-good/#gitlab-ranked-number-4-software-company-44th-overall-on-inc-5000-list-of-2018s-fastest-growing-companies)
* [GitLab has 2/3 market share in the self-managed Git market](/is-it-any-good/#gitlab-has-23-market-share-in-the-self-managed-git-market)
* [GitLab CI is the fastest growing CI/CD solution](/is-it-any-good/#gitlab-has-23-market-share-in-the-self-managed-git-market)
* [GitLab CI is a leader in The Forrester Wave™](/is-it-any-good/#gitlab-ci-is-a-leader-in-the-the-forrester-wave)
* [GitLab is a top 3 innovator in IDC's list of Agile Code Development Technologies for 2018](/is-it-any-good/#gitlab-is-a-top-3-innovator-in-idcs-list-of-agile-code-development-technologies-for-2018)
* [GitLab is a strong performer in the new Forrester Value Stream Management Tools 2018 Wave Report](/is-it-any-good/#gitlab-is-a-strong-performer-in-the-new-forrester-value-stream-management-tools-2018-wave-report)
* [GitLab is one of the top 30 open source projects](/is-it-any-good/#gitlab-is-one-of-the-top-30-open-source-projects)
* [GitLab has more than 2,000 contributors](/is-it-any-good/#gitlab-has-more-than-2000-contributors)
* [GitLab has been voted as G2 Crowd Leader in 2018](/is-it-any-good/#gitlab-has-been-voted-as-g2-crowd-leader-in-2018)
