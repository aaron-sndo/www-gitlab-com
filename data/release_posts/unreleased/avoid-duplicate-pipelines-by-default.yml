deprecations:
  - feature_name: "Change default workflow to avoid duplicate pipelines in merge requests"
    due: June 22, 2021
    reporter: jheimbuck_gl
    description: |
      Currently, it's possible to configure a pipeline in a way that an extra pipeline is triggered accidentally. Usually this is caused by configuration that allows both branch pipelines and merge requests pipelines to trigger simultaneously. With this configuration, pushing to a branch that also has an open merge request triggers two pipelines when you only wanted one. You can use [`workflow:rules` with the `CI_OPEN_MERGE_REQUESTS` variable](https://docs.gitlab.com/ee/ci/yaml/#switch-between-branch-pipelines-and-merge-request-pipelines) to allow both pipelines types in one configuration, without triggering extra pipelines.

      In GitLab 14.0, this `workflow:rules` will become the default behavior in pipelines with no `workflow:rules` configured. If this new default behavior is undesired, you can add your own `workflow:rules` to your pipeline configuration, which will override the new default and restore the original behavior. Pipelines that already use `workflow:rules` will be unaffected.

      Relevant Issue: [Make the workflow to avoid duplicate pipelines in merge requests the default configuration](https://gitlab.com/gitlab-org/gitlab/-/issues/300146)
